package rcontarini.com.br.soundtrackskate.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class SkaterVO implements Serializable {

    private Long idSkater;

    private String name;

    public Long getIdSkater() {
        return idSkater;
    }

    public void setIdSkater(Long idSkater) {
        this.idSkater = idSkater;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
