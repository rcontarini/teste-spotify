package rcontarini.com.br.soundtrackskate.mvp;

import java.util.ArrayList;
import java.util.List;

import rcontarini.com.br.soundtrackskate.domain.VideoVO;
import rcontarini.com.br.soundtrackskate.service.RetrofitInitializar;
import rcontarini.com.br.soundtrackskate.service.SoundTrackService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ModelImpl implements Model {

    private Presenter mPresenter;

    public ModelImpl(Presenter presenter){
        mPresenter = presenter;
    }


    @Override
    public void retrieveVideos() {
        mPresenter.showProgressBar(true);
        SoundTrackService soundTrackService = new RetrofitInitializar(mPresenter.getContext()).getSoundTrackService();
        final Call<List<VideoVO>> request = soundTrackService.getListVideos();
        request.enqueue(new Callback<List<VideoVO>>() {
            @Override
            public void onResponse(Call<List<VideoVO>> call, Response<List<VideoVO>> response) {
                mPresenter.showProgressBar(false);
                List<VideoVO> mListRetorno = new ArrayList<>();
                if(response.isSuccessful()){
                    mListRetorno = response.body();
                    mPresenter.updateListaRecycler(mListRetorno);
                }
            }

            @Override
            public void onFailure(Call<List<VideoVO>> call, Throwable t) {
                mPresenter.showProgressBar(false);
                mPresenter.showToast(t.getMessage());
            }
        });
    }
}
