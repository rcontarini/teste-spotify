package rcontarini.com.br.soundtrackskate.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class VideoVO implements Serializable {

    private Long idVideo;

    private String image;

    private String title;

    private String trailer;

    private String lenght;

    private String year;

    private String company;

    private VideoMakerVO videoMakerVO;

    private String country;

    private String category;

    private List<SoundTrackVO> mListSoundTrack;

    private List<SkaterVO> mListSkater;



    public Long getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(Long idVideo) {
        this.idVideo = idVideo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getLenght() {
        return lenght;
    }

    public void setLenght(String lenght) {
        this.lenght = lenght;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public VideoMakerVO getVideoMakerVO() {
        return videoMakerVO;
    }

    public void setVideoMakerVO(VideoMakerVO videoMakerVO) {
        this.videoMakerVO = videoMakerVO;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<SoundTrackVO> getmListSoundTrack() {
        return mListSoundTrack;
    }

    public void setmListSoundTrack(List<SoundTrackVO> mListSoundTrack) {
        this.mListSoundTrack = mListSoundTrack;
    }

    public List<SkaterVO> getmListSkater() {
        return mListSkater;
    }

    public void setmListSkater(List<SkaterVO> mListSkater) {
        this.mListSkater = mListSkater;
    }

}
