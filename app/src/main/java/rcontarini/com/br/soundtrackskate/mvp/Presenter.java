package rcontarini.com.br.soundtrackskate.mvp;

import android.content.Context;
import android.os.Bundle;

import java.util.List;

import rcontarini.com.br.soundtrackskate.domain.VideoVO;

public interface Presenter {
    public void retrieveVideos(Bundle savedInstanceState);
    public void showToast(String mensagem);
    public void showProgressBar(boolean status);
    public void setView(View view);
    public Context getContext();
    public void updateListaRecycler(List<VideoVO> mVideo);
    public void updateItemRecycler(VideoVO mVideo);
    public List<VideoVO> getVideos();
}
