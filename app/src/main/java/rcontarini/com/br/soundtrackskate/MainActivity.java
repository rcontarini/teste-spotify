package rcontarini.com.br.soundtrackskate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import rcontarini.com.br.soundtrackskate.adapter.VideosAdapter;
import rcontarini.com.br.soundtrackskate.mvp.Presenter;
import rcontarini.com.br.soundtrackskate.mvp.PresenterImpl;
import rcontarini.com.br.soundtrackskate.mvp.View;

public class MainActivity extends AppCompatActivity implements View {

    private VideosAdapter mAdapter;
    private Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(mPresenter == null){
            mPresenter = new PresenterImpl();
        }

        mPresenter.setView( this );
        mPresenter.retrieveVideos( savedInstanceState );
    }

    @Override
    protected void onStart() {
        super.onStart();

        RecyclerView rvVideos = findViewById(R.id.rv_videos);
        rvVideos.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvVideos.setLayoutManager( linearLayoutManager );

        mAdapter = new VideosAdapter( this, mPresenter.getVideos() );
        rvVideos.setAdapter( mAdapter );
    }

    @Override
    public void showToast(String mensagem) {
        Toast.makeText(this, mensagem, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressBar(int visibilidade) {
        findViewById(R.id.pb_loading).setVisibility( visibilidade );
    }

    @Override
    public void updateListaRecycler() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateItemRecycler(int posicao) {

    }
}
