package rcontarini.com.br.soundtrackskate.mvp;

public interface ModelSoundTrack {
    public void retrieveSoundTrack();

    String searchMusic(String search, String token);
}
