package rcontarini.com.br.soundtrackskate.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import rcontarini.com.br.soundtrackskate.MainActivity;
import rcontarini.com.br.soundtrackskate.R;
import rcontarini.com.br.soundtrackskate.SoundTrackActivity;
import rcontarini.com.br.soundtrackskate.domain.SoundTrackVO;
import rcontarini.com.br.soundtrackskate.domain.VideoVO;
import rcontarini.com.br.soundtrackskate.mvp.PresenterSoundTrack;


public class SoundTrackAdapter extends RecyclerView.Adapter<SoundTrackAdapter.ViewHolder> {


    private SoundTrackActivity soundTrackActivity;
    private List<SoundTrackVO> mSoundTrack;
    private PresenterSoundTrack mPresenter;

    public SoundTrackAdapter(SoundTrackActivity activity, List<SoundTrackVO> soundTrack, PresenterSoundTrack presenter){
        soundTrackActivity = activity;
        mSoundTrack = soundTrack;
        mPresenter = presenter;
    }

    @Override
    public SoundTrackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from( parent.getContext() )
                .inflate(R.layout.item_sound, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(SoundTrackAdapter.ViewHolder holder, int position) {
        holder.setDados( mSoundTrack.get( position ) );
    }

    @Override
    public int getItemCount() {
        return mSoundTrack.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
         TextView mSkater;
         TextView mBand;
         TextView mMusic;

        private ViewHolder(View itemView) {
            super(itemView);

            mSkater = itemView.findViewById(R.id.tv_skater);
            mBand = itemView.findViewById(R.id.tv_band);
            mMusic = itemView.findViewById(R.id.tv_mu);

            itemView.setOnClickListener(this);

        }

        private void setDados (SoundTrackVO soundTrackVO){
            mSkater.setText(soundTrackVO.getVideo_part());
            mBand.setText(soundTrackVO.getBand());
            mMusic.setText(soundTrackVO.getMusic());

        }

        @Override
        public void onClick(View v) {
            mPresenter.searchMusic(mMusic.getText().toString().concat("-")
                    .concat(mBand.getText().toString()), mPresenter.getToken());
        }
    }
}
