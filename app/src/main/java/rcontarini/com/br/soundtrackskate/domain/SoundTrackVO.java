package rcontarini.com.br.soundtrackskate.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class SoundTrackVO implements Serializable {

    private Long idSoundTrack;

    private String video_part;

    private String band;

    private String music;

    public Long getIdSoundTrack() {
        return idSoundTrack;
    }

    public void setIdSoundTrack(Long idSoundTrack) {
        this.idSoundTrack = idSoundTrack;
    }

    public String getVideo_part() {
        return video_part;
    }

    public void setVideo_part(String video_part) {
        this.video_part = video_part;
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }


}
