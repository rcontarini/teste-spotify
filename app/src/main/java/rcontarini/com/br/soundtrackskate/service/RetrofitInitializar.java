package rcontarini.com.br.soundtrackskate.service;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInitializar {

    private final Retrofit retrofit;

    public RetrofitInitializar(Context context){
        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/SoundTrack/")
                .client(clientInitializer())
                .addConverterFactory(getGsonConverterFactory())
                .build();
    }

    private OkHttpClient clientInitializer() {
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public SoundTrackService getSoundTrackService(){
        return retrofit.create(SoundTrackService.class);
    }

    private GsonConverterFactory getGsonConverterFactory() {
        return GsonConverterFactory.create(getJson());
    }


    private Gson getJson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setPrettyPrinting()
                .create();
    }
}
