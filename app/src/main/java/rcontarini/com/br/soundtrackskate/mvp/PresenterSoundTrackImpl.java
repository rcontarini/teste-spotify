package rcontarini.com.br.soundtrackskate.mvp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import rcontarini.com.br.soundtrackskate.domain.VideoVO;


public class PresenterSoundTrackImpl implements PresenterSoundTrack {

    private ModelSoundTrackImpl mModel;
    private ViewSoundTrack mView;
    private VideoVO mVideo = new VideoVO();

    public PresenterSoundTrackImpl(){
        mModel = new ModelSoundTrackImpl(this);
    }


    @Override
    public void retrieveSoundTrack(Bundle savedInstanceState) {
    }

    @Override
    public void showToast(String mensagem) {
        mView.showToast(mensagem);
    }

    @Override
    public void showProgressBar(boolean status) {
        int visibilidade = status ? android.view.View.VISIBLE : android.view.View.GONE;
        mView.showProgressBar( visibilidade );
    }

    @Override
    public void setView(ViewSoundTrack view) {
        mView = view;
    }

    @Override
    public Context getContext() {
        return (Context) mView;
    }

    @Override
    public void updateListaRecycler(List<VideoVO> mVideo) {
    }

    @Override
    public void updateItemRecycler(VideoVO mVideo) {
    }

    @Override
    public VideoVO getVideo() {
        return mVideo;
    }

    @Override
    public void searchMusic(String search, String token) {
        mModel.searchMusic(search, token);
    }

    @Override
    public String getToken() {
        return mView.getToken();
    }

    @Override
    public void preparePlayer() {
        mView.preparePlayer();
    }

    @Override
    public void player(String music) {
        mView.player(music);
    }
}
