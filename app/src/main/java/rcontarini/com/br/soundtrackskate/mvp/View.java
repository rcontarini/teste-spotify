package rcontarini.com.br.soundtrackskate.mvp;

public interface View {

    public void showToast(String mensagem);
    public void showProgressBar(int visibilidade);
    public void updateListaRecycler();
    public void updateItemRecycler(int posicao);
}
