package rcontarini.com.br.soundtrackskate.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class VideoMakerVO implements Serializable {

    private Long idVideoMaker;

    private String name;


    public Long getIdVideoMaker() {
        return idVideoMaker;
    }

    public void setIdVideoMaker(Long idVideoMaker) {
        this.idVideoMaker = idVideoMaker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
