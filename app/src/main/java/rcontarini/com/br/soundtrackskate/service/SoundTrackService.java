package rcontarini.com.br.soundtrackskate.service;

import java.util.List;

import rcontarini.com.br.soundtrackskate.domain.VideoVO;
import retrofit2.Call;
import retrofit2.http.GET;

public interface SoundTrackService {

    @GET("videoSoundTrack/getList")
    Call<List<VideoVO>> getListVideos();
}
