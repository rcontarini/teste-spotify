package rcontarini.com.br.soundtrackskate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.PlaybackState;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerEvent;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.SpotifyPlayer;

import rcontarini.com.br.soundtrackskate.adapter.SoundTrackAdapter;
import rcontarini.com.br.soundtrackskate.domain.VideoVO;
import rcontarini.com.br.soundtrackskate.mvp.PresenterSoundTrack;
import rcontarini.com.br.soundtrackskate.mvp.PresenterSoundTrackImpl;
import rcontarini.com.br.soundtrackskate.mvp.ViewSoundTrack;

public class SoundTrackActivity extends AppCompatActivity implements ViewSoundTrack, SpotifyPlayer.NotificationCallback, ConnectionStateCallback {

    private SoundTrackAdapter mAdapter;
    private PresenterSoundTrack mPresenter;
    YouTubePlayerView youtubePlayerView;
    private VideoVO videoVO;

    // TODO: Replace with your client ID
    private static final String CLIENT_ID = "cb20df687f6c46479b96c8c51ab22e18";
    // TODO: Replace with your redirect URI
    private static final String REDIRECT_URI = "http://localhost:8888/callback/";
    private static final int REQUEST_CODE = 1337;
    private String mToken;
    private Player mPlayer;
    private Config playerConfig;

    private FloatingActionButton mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_track);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        youtubePlayerView = findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youtubePlayerView);

        if(mPresenter == null){
            mPresenter = new PresenterSoundTrackImpl();
        }
        mPresenter.setView(this);

        //refatorar para add ButterKnife
        mFab = findViewById(R.id.fab);

        //refatorar para recuperar lista com o presenter, utilizando
        // padrao de projeto MVP
        Intent intent = getIntent();
        videoVO = (VideoVO) intent.getSerializableExtra("listSoundTrack");

        youtubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer youTubePlayer) {
                youTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        String videoId = "01234567912";
                        if(videoVO.getTrailer() != null && videoVO.getTrailer().length() > 0){
                            final String[] split = videoVO.getTrailer().split("/");
                            videoId = split[4];
                        }
                        youTubePlayer.cueVideo(videoId,0);
                        //youTubePlayer.loadVideo(videoId, 0);
                    }
                });
            }
        },true);
    }

    @Override
    protected void onStart() {
        super.onStart();


        RecyclerView rvVideos = findViewById(R.id.rv_sound);
        rvVideos.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvVideos.setLayoutManager( linearLayoutManager );

        mAdapter = new SoundTrackAdapter( this, videoVO.getmListSoundTrack(), mPresenter );
        rvVideos.setAdapter( mAdapter );
    }

    @Override
    public void showToast(String mensagem) {
        Toast.makeText(this, mensagem, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressBar(int visibilidade) {
        findViewById(R.id.pb_loading).setVisibility( visibilidade );
    }

    @Override
    public String getToken() {
        if(mToken == null) return null;
        return mToken;
    }

    @Override
    public void onLoggedIn() {
        showToast("User logged in");
        Log.d("MainActivity", "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.d("MainActivity", "User logged out");
    }

    @Override
    public void onLoginFailed(Error error) {
        Log.d("MainActivity", "Login failed");
    }

    @Override
    public void onTemporaryError() {
        Log.d("MainActivity", "Temporary error occurred");
    }

    @Override
    public void onConnectionMessage(String s) {
        Log.d("MainActivity", "Received connection message: " + s);
    }

    @Override
    public void onPlaybackEvent(PlayerEvent playerEvent) {
        Log.d("MainActivity", "Playback event received: " + playerEvent.name());
        switch (playerEvent) {
            // Handle event type as necessary
            default:
                break;
        }
    }

    @Override
    public void onPlaybackError(Error error) {
        showToast(error.name());
        Log.d("MainActivity", "Playback error received: " + error.name());
        switch (error) {
            // Handle error type as necessary
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);
            if (response.getType() == AuthenticationResponse.Type.TOKEN) {

                mToken = response.getAccessToken();
                playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
            }
        }
    }

    public void initAuth(View view){
        // The only thing that's different is we added the 5 lines below.
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
        builder.setScopes(new String[]{"user-read-private", "streaming"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }

    public void preparePlayer(){
        Spotify.getPlayer(playerConfig, this, new SpotifyPlayer.InitializationObserver() {
            @Override
            public void onInitialized(SpotifyPlayer spotifyPlayer) {
                mPlayer = spotifyPlayer;
                mPlayer.addConnectionStateCallback(SoundTrackActivity.this);
                mPlayer.addNotificationCallback(SoundTrackActivity.this);
            }
            @Override
            public void onError(Throwable throwable) {
                Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
            }
        });
    }

    public void player(String music){
        String player = "spotify:track:".concat(music);
        mPlayer.playUri(null, player, 0, 0);
    }

}
