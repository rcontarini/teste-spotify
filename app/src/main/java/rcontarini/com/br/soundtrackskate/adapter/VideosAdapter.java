package rcontarini.com.br.soundtrackskate.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.List;

import rcontarini.com.br.soundtrackskate.MainActivity;
import rcontarini.com.br.soundtrackskate.R;
import rcontarini.com.br.soundtrackskate.SoundTrackActivity;
import rcontarini.com.br.soundtrackskate.domain.VideoVO;


public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {


    private MainActivity mainActivity;
    private List<VideoVO> mVideos;

    public VideosAdapter(MainActivity activity, List<VideoVO> listVideo){
        mainActivity = activity;
        mVideos = listVideo;
    }

    @Override
    public VideosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from( parent.getContext() )
                .inflate(R.layout.item_video, parent, false);
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(VideosAdapter.ViewHolder holder, int position) {
        holder.setDados( mVideos.get( position ) );
    }

    @Override
    public int getItemCount() {
        return mVideos.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivCapa;
        private TextView tvTitle;
        private TextView tvVideoMaker;
        private TextView tvYear;

        private ViewHolder(View itemView) {
            super(itemView);

            ivCapa = itemView.findViewById(R.id.iv_capa);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvVideoMaker = itemView.findViewById(R.id.tv_videoMaker);
            tvYear = itemView.findViewById(R.id.tv_year);

            itemView.setOnClickListener( this );

        }

        private void setDados (VideoVO videoVO){

            if(videoVO.getImage() != null && videoVO.getImage().length() > 0){
                String UrlFoto = "http://www.skatevideosite.com";
                String nomeImagem = videoVO.getImage().replace("amp;","");
                String esse = UrlFoto.concat(nomeImagem);
                String teste = esse.substring(0,esse.length()-1);
                Log.i("caminhoFoto", UrlFoto.concat(nomeImagem));


                Picasso.with( mainActivity )
                        .load( teste )
                        .resize(150,200)
                        .error(R.drawable.fab_add)
                        .into( ivCapa );

                tvTitle.setText( videoVO.getTitle() );
                tvYear.setText( videoVO.getYear() );
                if(videoVO.getVideoMakerVO() != null && videoVO.getVideoMakerVO().getName().length() > 0){
                    tvVideoMaker.setText( videoVO.getVideoMakerVO().getName() );
                }

            }

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mainActivity, SoundTrackActivity.class);

            VideoVO videoVO = mVideos.get(getAdapterPosition());

            intent.putExtra("listSoundTrack", videoVO);
            mainActivity.startActivity(intent);
        }
    }
}
