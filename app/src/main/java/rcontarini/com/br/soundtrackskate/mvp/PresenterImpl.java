package rcontarini.com.br.soundtrackskate.mvp;

import android.content.Context;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import rcontarini.com.br.soundtrackskate.domain.VideoVO;


public class PresenterImpl implements Presenter {

    private ModelImpl mModel;
    private View mView;
    private List<VideoVO> mListVideos = new ArrayList<>();

    public PresenterImpl (){
        mModel = new ModelImpl(this);
    }


    @Override
    public void retrieveVideos(Bundle savedInstanceState) {
        mModel.retrieveVideos();
    }

    @Override
    public void showToast(String mensagem) {
        mView.showToast(mensagem);
    }

    @Override
    public void showProgressBar(boolean status) {
        int visibilidade = status ? android.view.View.VISIBLE : android.view.View.GONE;
        mView.showProgressBar( visibilidade );
    }

    @Override
    public void setView(View view) {
        mView = view;
    }

    @Override
    public Context getContext() {
        return (Context) mView;
    }

    @Override
    public void updateListaRecycler(List<VideoVO> mVideo) {
        mListVideos.clear();
        mListVideos.addAll( mVideo );
        mView.updateListaRecycler();

    }

    @Override
    public void updateItemRecycler(VideoVO mVideo) {

    }

    @Override
    public List<VideoVO> getVideos() {
        return mListVideos;
    }
}
