package rcontarini.com.br.soundtrackskate.mvp;

import android.util.Log;
import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.TracksPager;
import retrofit.RetrofitError;


public class ModelSoundTrackImpl implements ModelSoundTrack {

    private PresenterSoundTrack mPresenter;

    public ModelSoundTrackImpl(PresenterSoundTrack presenter){
        mPresenter = presenter;
    }


    @Override
    public void retrieveSoundTrack() {
    }

    @Override
    public String searchMusic(String search, String token) {
        try {
            SpotifyApi spotifyApi = new SpotifyApi();
            spotifyApi.setAccessToken(token);
            SpotifyService service = spotifyApi.getService();

            service.searchTracks(search, new retrofit.Callback<TracksPager>() {
                @Override
                public void success(TracksPager tracksPager, retrofit.client.Response response) {
                    Log.i("Album Sucess", tracksPager.tracks.items.get(0).name);
                    mPresenter.showToast(tracksPager.tracks.items.get(0).name);
                    mPresenter.preparePlayer();
                    mPresenter.player(tracksPager.tracks.items.get(0).id);
                }

                @Override
                public void failure(RetrofitError error) {
                    mPresenter.showToast(error.getMessage());
                    Log.e("Album Error", error.getMessage());
                }
            });

        } catch (Exception e){
            Log.e("LOG", e.getMessage());
        }
        return null;
    }
}
