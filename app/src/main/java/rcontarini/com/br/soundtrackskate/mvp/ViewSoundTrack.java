package rcontarini.com.br.soundtrackskate.mvp;

public interface ViewSoundTrack {

    public void showToast(String mensagem);
    public void showProgressBar(int visibilidade);
    public String getToken();
    public void preparePlayer();
    public void player(String music);

}
